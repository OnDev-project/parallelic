from zmq import PUB, SUB, ROUTER, DEALER
from parallelic.util.connector import *


def test_manager_sockets():
    reporter, notifier = manager_bind()
    assert reporter.type == ROUTER
    assert notifier.type == PUB


def test_runner_sockets():
    reporter, notifier = runner_connect()
    assert reporter.type == DEALER
    assert notifier.type == SUB