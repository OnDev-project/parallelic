__version__ = "0.1.3"

def main(**kwargs):
    from sys import argv
    from parallelic.util.utils import args_to_kv
    kwargs.update(args_to_kv(*argv))
    if kwargs.get("execMode") == "manager":
        from parallelic.manager import main
    if kwargs.get("execMode", "runner") == "runner":
        from parallelic.runner import main
    return main(**kwargs)
