from .recv import receive
from .exec import run_cmd

def main(**kwargs):
    if kwargs.get("dryRun", False):
        return 0
    task = receive()
