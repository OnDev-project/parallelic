from subprocess import Popen, PIPE
from os.path import join
from ..util import parser
from .reporter import task_report


def run_cmd(cmd, context="default"):
    with Popen(
        cmd, stdout=PIPE, stderr=PIPE, bufsize=1, universal_newlines=True
    ) as process:
        for line in process.stdout:
            task_report(line, context)
        return process.poll()


def run_task(task):
    print(task)
    parser.get_setup_check(join(task, "task.toml"))
