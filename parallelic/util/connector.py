from os import getenv
import zmq

REPORTER_PORT = getenv("REPORTER_PORT", 5556)
NOTIFIER_PORT = getenv("NOTIFIER_PORT", 5557)

ctx = zmq.Context()

def manager_bind():
    reporter_server = ctx.socket(zmq.ROUTER)
    reporter_server.bind(f"tcp://*:{REPORTER_PORT}")

    notifier_server = ctx.socket(zmq.PUB)
    notifier_server.bind(f"tcp://*:{NOTIFIER_PORT}")

    return (reporter_server, notifier_server)


def runner_connect():
    reporter_client = ctx.socket(zmq.DEALER)
    reporter_client.connect(f"tcp://{getenv('MANAGER_IP')}:{REPORTER_PORT}")

    notifier_client = ctx.socket(zmq.SUB)
    notifier_client.connect(f"tcp://{getenv('MANAGER_IP')}:{NOTIFIER_PORT}")

    return (reporter_client, notifier_client)

